module.exports = {
	preset: "ts-jest",
	testEnvironment: "node",
	rootDir: "src",
	reporters: ["jest-junit"],
}
