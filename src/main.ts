import "core-js"

import * as express from "express"
import * as helmet from "helmet"

import {
	appsRouter,
	domainsRouter,
	cronsRouter,
	variablesRouter,
	usersRouter,
	meRouter,
	setupRouter,
	accessKeyRouter,
	mobileconfigRouter,
	versionRouter,
	restartRouter,
} from "./routes"
import { getUser } from "./lib"
import { logger } from "./tools"

const PRODUCTION = process.env.NODE_ENV === "production"
const PORT = process.env.PORT || 80
const PRIMARY_DOMAIN_NAME = process.env.PRIMARY_DOMAIN_NAME || ""

// Create express app
const app = express()

// Add some security HTTP headers
// https://github.com/helmetjs/helmet
app.use(helmet({
	contentSecurityPolicy: {
		directives: {
			defaultSrc: ["'self'"],
			baseUri: ["'self'"],
			fontSrc: ["'self'", "https:", "data:"],
			frameAncestors: ["'self'"],
			imgSrc: ["'self'", "data:", `*.${PRIMARY_DOMAIN_NAME}`],
			objectSrc: ["'none'"],
			scriptSrc: ["'self'"],
			scriptSrcAttr: ["'none'"],
			styleSrc: ["'self'", "https:", "'unsafe-inline'"],
		}
	}
}))
// blockAllMixedContent: true,
// upgradeInsecureRequests: true


// Parse JSON bodies and put it in request.body.
// https://github.com/expressjs/body-parser
app.use(express.json())
// Same but for URL encoded body
app.use(express.urlencoded({ extended: true }))

// Use Remote-User as authentication.
app.use(async (request, _response, next) => {
	const remoteUser = request.header("Remote-User")

	if (remoteUser !== undefined) {
		try {
			; (request as any).user = await getUser(remoteUser)
		} catch (error: any) { }
	}

	next()
})

// FLAP home API
app.use(
	"/api",
	express
		.Router()
		// API's routes
		.use("/accessKey", accessKeyRouter)
		.use("/apps", appsRouter)
		.use("/crons", cronsRouter)
		.use("/domains", domainsRouter)
		.use("/variables", variablesRouter)
		.use("/me", meRouter)
		.use("/mobileconfig", mobileconfigRouter)
		.use("/restart", restartRouter)
		.use("/setup", setupRouter)
		.use("/users", usersRouter)
		.use("/version", versionRouter),
)

// Custom error handler
app.use(
	(
		error: { code: number; message: string | Error } | string | Error,
		request: express.Request,
		response: express.Response,
		_next: any,
	) => {
		// Default HTTP code to 500
		if (typeof error === "string" || error instanceof Error) {
			error = { code: 500, message: error }
		}

		// Wrap the message into an Error object
		if (!(error.message instanceof Error)) {
			error.message = new Error(error.message)
		}

		// Log for debugging
		logger.error(
			`${request.method} ${(request.route || {}).path} - ${
			error.code
			} - `,
			error.message,
		)

		if (!PRODUCTION) {
			logger.error(error)
		}

		response.setHeader("Content-type", "text/plain")

		if (PRODUCTION) {
			response.status(error.code)
		} else {
			response.status(error.code).send(error.message.message)
		}

		response.end()
	},
)

// Return a 404 for inexistant /api endpoints
app.use("/api/*", (_request, response) => {
	response.status(404).send("This API endpoint does not exists.")
})

// Front files
app.use("/", express.static("./src/front/dist"))
// Fallback to index.html to mimic historyAPIFallback
app.use("*", express.static("./src/front/dist/index.html"))

// Start listening
app.listen(PORT, () => logger.info(`Listening on port ${PORT}`))
