import { promises as fs } from "fs"

export async function readFile(file: string) {
	return (await fs.readFile(file, "utf8")).trim()
}
