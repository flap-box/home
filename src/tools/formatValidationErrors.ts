import { Result } from "express-validator/check"

export function formatValidationErrors(error: Result<{}>) {
	return new Error(JSON.stringify(error.mapped()))
}
