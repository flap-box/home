export interface IApp {
	name: string
	enabled: boolean
	subdomain: string
	path: string
	icon: string
}
