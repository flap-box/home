import * as fs from "fs-extra"
import * as dotenv from "dotenv"

import { readFile } from "../tools"
import {
	Variable,
	VariableInfo,
	VariableType,
	VariableTypeString,
} from "../front/src/models"

export async function getVariables(): Promise<{ [key: string]: Variable }> {
	const flapctl = await readFile(`/var/lib/flap/flapctl.env`)

	// Remove 'export ' because dotenv does not work with them.
	const variablesValue = dotenv.parse(
		Buffer.from(flapctl.replace(/export /g, "")),
	)

	const variablesInfo: { [key: string]: VariableInfo } = JSON.parse(
		await readFile(`/var/lib/flap/variables.json`),
	)

	return Object.keys(variablesInfo).reduce((vars, name) => {
		const info = variablesInfo[name]
		const value = convertVariable(variablesValue[name], info.type)

		return {
			...vars,
			[name]: { name, value, ...info },
		}
	}, {} as { [key: string]: Variable })
}

function convertVariable(
	value: string | undefined,
	type: VariableTypeString,
): VariableType {
	if (value === undefined) {
		switch (type) {
			case "string":
				return ""
			case "boolean":
				return false
			default:
				return ""
		}
	} else {
		switch (type) {
			case "string":
				return value
			case "boolean":
				return value == "true"
			default:
				return ""
		}
	}
}

export async function getVariable(name: string): Promise<VariableType> {
	const variable = (await getVariables())[name]

	if (!variable) {
		throw new Error("Variables does not exists")
	}

	return variable.value
}

export async function setVariables(variables: { [key: string]: Variable }) {
	await fs.writeFile(
		`/var/lib/flap/flapctl.env`,
		Object.values(variables)
			.filter(({ value }) => value !== false) // Do not write false values to keep flapctl.env clean.
			.map(({ name, value }) => `export ${name}=${value}`)
			.join("\n"),
	)
}
