import * as crypto from "crypto"
import { promises as fs } from "fs"

import { Change } from "ldapjs"
import * as nodemailer from "nodemailer"
import * as Handlebars from "handlebars"

import { logger } from "../tools"
import * as ldap from "./ldap"
import { IUser } from "../front/src/models"
import { getDomains } from "./domains"

const LDAP_HOST = process.env.LDAP_HOST || "ldap://localhost"
const LDAP_BASE = process.env.LDAP_BASE || "ou=users,dc=flap,dc=local"
const LDAP_ADMIN_DN = process.env.LDAP_ADMIN_DN || "cn=admin,dc=flap,dc=local"
const LDAP_ADMIN_PWD = process.env.LDAP_ADMIN_PWD || "admin"

const PRIMARY_DOMAIN_NAME = process.env.PRIMARY_DOMAIN_NAME

const EMAIL_SMTP_USER = process.env.EMAIL_SMTP_USER
const EMAIL_SMTP_PASSWORD = process.env.EMAIL_SMTP_PASSWORD

const DEFAULT_USER_CLASSES = [
	"person", // cn, sn, userPassword
	"inetOrgPerson", // mail
	"PostfixBookMailAccount", // mailAlias
]

export async function searchUsers(): Promise<IUser[]> {
	logger.debug(`Searching users in LDAP server`)
	// Bind to the LDAP server
	const client = await ldap.bind(LDAP_HOST, LDAP_ADMIN_DN, LDAP_ADMIN_PWD)
	// Query the server
	const entries = await ldap.search(client, LDAP_BASE)
	// Unbind from the LDAP server
	ldap.unbind(client)

	// Map users to our IUser interface
	const users = entries.map((entry: any) => ({
		fullname: entry.object.cn,
		username: entry.object.uid,
		email: entry.object.mail,
		admin: entry.object.objectClass.includes("organizationalPerson"),
	}))

	logger.debug(`Searching users in LDAP server`)
	logger.debug(`Found ${users.length} users: ${JSON.stringify(users)}`)

	return users
}

// TODO - this should be improved so we don't have to loop through all the users
export async function getUser(username: string): Promise<IUser> {
	logger.debug(`Getting user ${username}`)
	// Get all users
	const users = await searchUsers()
	// Search for the user with the passed username
	const user = users.find(user => user.username === username)

	if (user === undefined) {
		throw new Error(`The user '${username}' does not exists`)
	}

	logger.debug(`User found ${JSON.stringify(user)}`)

	return user
}

export async function createUser(
	params: IUser & { password: string },
): Promise<IUser> {
	let shoudlSendPassword = false

	// Generate username from fullname if username is empty.
	if (params.username === undefined) {
		params.username = params.fullname
			.trim()
			.split(" ")[0]
			.trim()
			.toLowerCase()
			.split("")
			.filter(char => char.search(/[a-z]/) != -1)
			.join("")
			.substr(0, 32)

		if (params.username == "admin") {
			throw new Error("username can not be 'admin'")
		}

		logger.debug("Username derived from fullname")
	}

	// Generate tmp password if password is missing.
	if (params.password === undefined) {
		logger.debug("Generating tmp password")
		shoudlSendPassword = true
		params.password = crypto.randomBytes(20).toString("hex")
	}

	logger.debug(
		`Creating a new user ${params.username} with fullname: ${params.fullname} `,
	)

	// Bind to the LDAP server
	const client = await ldap.bind(LDAP_HOST, LDAP_ADMIN_DN, LDAP_ADMIN_PWD)

	const mailAliases = await getMailAliases(params.username)

	// Send the new entry to the LDAP server
	await ldap.add(client, `uid=${params.username},${LDAP_BASE}`, {
		objectClass: [
			...DEFAULT_USER_CLASSES,
			...(params.admin ? ["organizationalPerson"] : []),
		],
		uid: params.username,
		sn: params.username,
		cn: params.fullname,
		mail: params.email,
		...(mailAliases.length > 0 ? { mailAlias: mailAliases } : {}),
		userPassword: params.password,
	})

	// Change password with exop so it is not stored in plain text.
	await ldap.changePwd(
		client,
		`uid=${params.username},${LDAP_BASE}`,
		params.password,
		params.password,
	)

	// Unbind from the LDAP server
	ldap.unbind(client)

	// Return the created user by searching it with its username
	const user = await getUser(params.username)

	if (shoudlSendPassword) {
		await sendPassword(params.email, user, params.password)
	}

	return user
}

// Allow the user to change its fullname and password.
// The username is imutable because it implies changing the username in all services, which can break things.
export async function updateUser(
	username: string,
	params: Partial<{
		fullname: string
		email: string
		admin: boolean
	}>,
): Promise<IUser> {
	logger.debug(`Updating user ${username} with`, params)
	let changes: Change[] = []

	// Always refresh mail aliases on an update.
	changes.push(
		new Change({
			operation: "replace",
			modification: {
				mailAlias: await getMailAliases(username),
			},
		}),
	)

	if (params.fullname) {
		// Build Change object for the fullname
		changes.push(
			new Change({
				operation: "replace",
				modification: {
					cn: params.fullname,
				},
			}),
		)
	}

	if (params.email) {
		// Build Change object for the email
		changes.push(
			new Change({
				operation: "replace",
				modification: {
					mail: params.email,
				},
			}),
		)
	}

	if (params.admin !== undefined) {
		// Build Change object for the email
		changes.push(
			new Change({
				operation: "replace",
				modification: {
					objectClass: [
						...DEFAULT_USER_CLASSES,
						...(params.admin ? ["organizationalPerson"] : []),
					],
				},
			}),
		)
	}

	// Bind to the LDAP server
	const client = await ldap.bind(LDAP_HOST, LDAP_ADMIN_DN, LDAP_ADMIN_PWD)
	// Send the changes to the LDAP server
	await ldap.modify(client, `uid=${username},${LDAP_BASE}`, changes)
	// Unbind from the LDAP server
	ldap.unbind(client)

	return await getUser(username)
}

export async function updatePassword(
	username: string,
	params: {
		oldPassword: string
		newPassword: string
	},
): Promise<void> {
	// Bind to the LDAP server
	const client = await ldap.bind(LDAP_HOST, LDAP_ADMIN_DN, LDAP_ADMIN_PWD)

	// Change password with exop so it is not stored in plain text.
	await ldap.changePwd(
		client,
		`uid=${username},${LDAP_BASE}`,
		params.oldPassword,
		params.newPassword,
	)

	// Unbind from the LDAP server
	ldap.unbind(client)
}

export async function deleteUser(username: string): Promise<void> {
	logger.debug(`Deleting user ${username}`)
	// Bind to the LDAP server
	const client = await ldap.bind(LDAP_HOST, LDAP_ADMIN_DN, LDAP_ADMIN_PWD)
	// Send the deletion order the LDAP server
	await ldap.del(client, `uid=${username},${LDAP_BASE}`)
	// Unbind from the LDAP server
	ldap.unbind(client)
}

export async function getUserWithCredentials(
	username: string,
	password: string,
): Promise<IUser | undefined> {
	logger.debug(`Getting the user ${username} with its credentials`)
	// Bind to the LDAP server with the user's credentials.
	const client = await ldap.bind(
		LDAP_HOST,
		`uid=${username},${LDAP_BASE}`,
		password,
	)

	// Query the server.
	const entries = await ldap.search(client, LDAP_BASE, `(uid=${username})`)

	// Unbind from the LDAP server
	ldap.unbind(client)

	// Map users to our IUser interface
	const user = entries.map((entry: any) => ({
		fullname: entry.object.cn,
		username: entry.object.uid,
		email: entry.object.mail,
		admin: entry.object.objectClass.includes("organizationalPerson"),
	}))[0]

	logger.debug(`User found: ${JSON.stringify(user)}`)

	return user
}

async function sendPassword(mail: string, user: IUser, password: string) {
	logger.debug("Sending password to user")

	const smtpTransport = nodemailer.createTransport({
		host: "mail",
		port: 587,
		auth: {
			user: EMAIL_SMTP_USER,
			pass: EMAIL_SMTP_PASSWORD,
		},
		tls: {
			rejectUnauthorized: false
		},
	})

	const template = Handlebars.compile(await fs.readFile("./src/templates/account-creation-template.html", "utf8"));

	try {
		await smtpTransport.sendMail({
			from: `${EMAIL_SMTP_USER}@${PRIMARY_DOMAIN_NAME}`,
			to: mail,
			subject: "[FLAP] Votre mot de passe.",
			html: template({ ...user, password, domain: PRIMARY_DOMAIN_NAME })
		})
	} finally {
		smtpTransport.close()
	}
}

async function getMailAliases(username: string) {
	const domains = await getDomains()
	return domains
		.filter(domain => domain.status === "OK")
		.map(domain => `${username}@${domain.name} `)
}
