import { AxiosError } from "axios"

export function errorToString(error: any) {
	if (typeof error === "string") {
		return error
	} else if (error instanceof Error) {
		if (isAxiosError(error)) {
			return `(${error.response.status}) - ${error.response.data}`
		} else {
			return error.message
		}
	} else {
		return `Error inconnue ${JSON.stringify(error)}`
	}
}

export function isAxiosError(error: Error): error is AxiosError {
	return (error as AxiosError).isAxiosError === true
}
