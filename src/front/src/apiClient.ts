import axios from "axios"
import { sleep } from "./tools"

export const apiClient = axios.create({
	baseURL: `${location.origin}/api`,
})

// Create an ever alive connection to a websocket.
// It will try to reconnect every second until the returned function is called.
// export function wsConnect(
// 	path: string,
// 	onMessage: (messageEvent: MessageEvent) => void,
// ) {
// 	let ws: WebSocket

// 	// Attempt to create a new connection every 5s when there is no connection alive.
// 	const interval = setInterval(() => {
// 		if (ws !== undefined && ws.readyState !== ws.CLOSED) {
// 			return
// 		}

// 		switch (location.protocol) {
// 			case "http:":
// 				ws = new WebSocket(`ws://${location.host}/api/${path}`)
// 				break
// 			case "https:":
// 				ws = new WebSocket(`wss://${location.host}/api/${path}`)
// 				break
// 		}

// 		ws.onmessage = onMessage
// 	}, 1000)

// 	return () => {
// 		if (ws !== undefined && ws.readyState !== ws.CLOSED) {
// 			ws.close()
// 		}

// 		clearInterval(interval)
// 	}
// }
