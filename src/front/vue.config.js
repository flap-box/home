const fs = require("fs")
const CopyPlugin = require("copy-webpack-plugin")

process.env.VUE_APP_VERSION = process.env.GIT_TAG

module.exports = {
	configureWebpack: {
		devtool: "inline-source-map",
		devServer:
			process.env.NODE_ENV === "development"
				? {
					https: {
						key: fs.readFileSync(
							"/etc/letsencrypt/live/flap/privkey.pem",
						),
						cert: fs.readFileSync(
							"/etc/letsencrypt/live/flap/fullchain.pem",
						),
						ca: fs.readFileSync(
							"/etc/letsencrypt/live/flap/chain.pem",
						),
					},
					client: {
						overlay: true,
					},
					devMiddleware: {
						public: "flap.test:8081",
						writeToDisk: true,
					}
				}
				: {},
		plugins: [
			new CopyPlugin({
				patterns: [
					{
						from: "node_modules/material-design-icons-iconfont/dist/fonts/MaterialIcons-Regular.woff2",
						to: "fonts",
					},
					{
						from: "node_modules/material-design-icons-iconfont/dist/fonts/MaterialIcons-Regular.woff",
						to: "fonts",
					},
					{
						from: "node_modules/material-design-icons-iconfont/dist/fonts/MaterialIcons-Regular.eot",
						to: "fonts",
					},
					{
						from: "node_modules/material-design-icons-iconfont/dist/fonts/MaterialIcons-Regular.ttf",
						to: "fonts",
					},
				],
			}),
		],
	},

	css: {
		loaderOptions: {
			css: {
				url: false,
			},
			sass: {
				additionalData: `@import "./src/styles/variables.scss";`,
			},
		},
	},

	pluginOptions: {
		i18n: {
			locale: "fr",
			fallbackLocale: "en",
			localeDir: "locales",
			enableInSFC: false,
		},
	},
}
