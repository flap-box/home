# Nextcloud

---

Nextcloud vous permet de stocker des fichiers dans votre compte FLAP.

## Synchronisez et accéder à vos fichiers

Nextcloud vous permet d&#39;accéder à vos fichiers grâce à des applications pour tous vos appareils.

[Il vous suffit de télécharger les applications proposées à ce lien](https://nextcloud.com/install/#install-clients).

Lors de la connexion, il vous faudra indiquer le nom de domaine suivant:

> `https://files.{{domain_name}}`

## Fonctionnalités clés

- Synchroniser les dossiers sélectionnés sur votre ordinateur en permanence.
- Partager vos fichiers, et dossiers avec des utilisateurs internes ou externes.
- Créer un dossier de dépôt pour recevoir des fichiers.
- Éditer des fichiers collaborativement en ligne.
- Garder un historique des versions de vos fichiers.

[Vous trouverez ici plus de fonctionnalités](https://nextcloud.com/files/).
