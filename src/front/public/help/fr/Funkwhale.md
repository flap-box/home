# Funkwhale

---

Funkwhale vous permet de publier des musiques.

## Vous connecter

La connexion n'est pas automatique pour Funkwhale. En cliquant sur l&#39;image de l&#39;application vous arriverez sur la page de connection de Funkwhale. Il vous suffira de rentrer vos identifiant et mot de passe puis de cliquer sur Login.

## Le réseau ActivityPub

Votre serveur Funkwhale est connecté au réseau ActivityPub. Ce réseau est une fédération de plateformes sociales de petite ou moyenne taille comme la votre. Cette fédération de plateformes permet aux quelques millions d&#39;utilisateurs qui l&#39;utilisent de réagir, commenter, et partager le contenu d&#39;instances qui n&#39;est pas la leur.

## Applications mobiles

Vous trouverez ici une liste d'applications compatible avec Funkwhale: <https://funkwhale.audio/fr_FR/apps>

## Plus d'informations

Vous trouverez plus d'information sur Funkwhale à l'adresse suivante: <https://funkwhale.audio/>
