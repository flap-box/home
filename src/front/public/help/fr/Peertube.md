# Peertube

---

Peertube vous permet de publier des vidéos. Son fonctionnement est très proche de celui de Youtube, à la différence que vous êtes en total contrôle du type de vidéos publiées. De plus, Peertube n&#39;affiche pas de publicités à vos auditeurs.

## Vous connecter

En cliquant sur l&#39;image de l&#39;application vous serez redirigé vers la page de login. Il vous suffira alors de cliquer sur le bouton "FLAP SSO ({{domain_name}})"

## Le réseau ActivityPub

Votre serveur Peertube est connecté au réseau ActivityPub. Ce réseau est une fédération de plateformes sociales de petite ou moyenne taille comme la vôtre. Cette fédération de plateformes permet aux quelques millions d&#39;utilisateurs qui l&#39;utilisent de réagir, commenter, et partager le contenu d&#39;instances qui n&#39;est pas la leur.

## Application Android

Vous pouvez utiliser l&#39;application [NewPipe](https://newpipe.net). Cette application est un lecteur de vidéos fonctionnant avec tous les serveurs Peertube, ainsi qu&#39;avec Youtube.

## Plus d'informations

Vous trouverez plus d'information sur Peertube à l'adresse suivante: https://joinpeertube.org/#what-is-peertube
