# Matomo

---

Matomo peut vous fournir un compte rendu de la fréquentation et de l&#39;utilisation de vos sites internet, de vos logiciels pour bureau ainsi que pour vos applications mobiles.

## Vous connecter

La connexion à Matomo n&#39;est pas automatique comme pour les autres services. Il vous faudra simplement indiquer votre nom d&#39;utilisateur et votre mot de passe sur la page de connexion.

## Retirer la bannière de recueillement de consentement

En utilisant Matomo vous ne partagez plus la responsabilité de traitement de données récupérées via les cookies. Il vous suffit de respecter les conditions suivantes afin de retirer cette ennuyeuse bannière de consentement.

- Avoir une finalité strictement limitée à la seule mesure de l’audience du site, et cela uniquement pour votre compte.
- Ne pas permettre le suivi global de la navigation de la personne utilisant différentes applications ou naviguant sur différents sites web.
- Servir uniquement à produire des données statistiques anonymes.
- Ne pas conduire à un recoupement des données avec d’autres traitements ou à ce que les données soient transmises à des tiers.
