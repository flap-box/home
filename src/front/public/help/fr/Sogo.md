# SOGo

---

SOGo est un logiciel permettant d&#39;envoyer des mail à partir de votre serveur FLAP mais aussi de gérer vos calendriers, contacts.

SOGo utilise certains standards vous permettant d&#39;accéder vos données sur vos différents appareil.

Ces standards sont les protocoles IMAP et SMTP pour les mails, CalDav pour les calendriers et CardDav pour les contacts.

## Accéder à vos mail sur vos appareils

La plupart des applications mail vous permettent de vous connecter à un serveur de mail en indiquant votre adresse mail et votre mot de passe.

Si besoin, voici les paramètres de connexion.

| Serveur SMTP        | Serveur IMAP        | Sécurité | Nom d&#39;utilisateur | Mot de passe |
| :------------------ | :------------------ | :------- | :-------------------- | :----------- |
| {{domain_name}}:587 | {{domain_name}}:143 | STARTTLS | {{username}}          | ...........  |

## Synchroniser vos calendriers et contacts sur vos appareils

La plupart des applications permettant de synchroniser vos calendrier et contacts vous permettent de vous connecter à un serveur CalDav et CardDav indiquant votre nom d&#39;utilisateur et votre mot de passe.

| Serveur         | Nom d&#39;utilisateur | Mot de passe |
| :-------------- | :-------------------- | :----------- |
| {{domain_name}} | {{username}}          | ...........  |

Si la connexion échoue, il est possible qu&#39;il soit nécessaire d&#39;utiliser le nom de domaine suivant: `https://mail.{{domain_name}}/SOGo/dav`

### MacOS & iOS

Vous pouvez télécharger votre profile de configuration dans les [réglages de votre compte FLAP](https://{{domain_name}}/settings).

### Android

Vous pouvez utiliser l&#39;application [DAVx5](https://www.davx5.com).

### Linux (Gnome)

Vous pouvez utiliser l&#39;application [Évolution](https://wiki.gnome.org/Apps/Evolution/).
