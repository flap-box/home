# Mailman

---

Mailman est un logiciel de gestion de listes mail. Mailman vous permet de créer des listes mail publiques ou privées et des listes de diffusion.

## Vous connecter

La connexion à Mailman n&#39;est pas automatique comme pour les autres services. Il vous faudra simplement indiquer votre nom d&#39;utilisateur et votre mot de passe sur la page de connexion.
