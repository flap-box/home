import * as express from "express"
import { body, validationResult } from "express-validator"
const mobileconfig = require("@artonge/mobileconfig")

import { getUserWithCredentials } from "../lib"
import { formatValidationErrors } from "../tools"
import { IUser } from "../front/src/models"

export const mobileconfigRouter = express.Router()

// /mobileconfig
mobileconfigRouter.route("/").post(
	// Check that submited properties are valid.
	body("password").isLength({ min: 8, max: 256 }),
	async (request, response, next) => {
		// Validate values.
		try {
			validationResult(request).throw()
		} catch (error: any) {
			return next({
				code: 400,
				message: formatValidationErrors(error),
			})
		}

		// The user needs to provide its password as a security measure.
		// This is also usefull to fill up the deviceprofile.
		try {
			;(request as any).user = await getUserWithCredentials(
				((request as any).user as IUser).username,
				request.body.password,
			)
		} catch (error: any) {
			return next({
				code: 401,
				message: `Wrong password.`,
			})
		}

		// Build the device profile.
		const profile = new mobileconfig.MobileConfigProfile({
			displayName: `FLAP box (${
				((request as any).user as IUser).username
			}@${request.hostname})`,
			description: "Sync your calendars and contacts to your FLAP box.",
			identifier: "cloud.flap.profile",
			organization: "FLAP",
		})

		// Add calendars.
		profile.addPayload(
			new mobileconfig.CalDAVPayload({
				identifier: "cloud.flap.profile",
				displayName: "FLAP's calendars",
				accountDescription: "Calendars from your FLAP box.",
				hostname: `mail.${request.hostname}`,
				username: ((request as any).user as IUser).username,
				password: request.body.password,
				useSSL: true,
				port: 443,
			}),
		)

		// Add contacts.
		profile.addPayload(
			new mobileconfig.CardDAVPayload({
				identifier: "cloud.flap.profile",
				displayName: "FLAP's contacts",
				accountDescription: "Contacts from your FLAP box.",
				hostname: `mail.${request.hostname}`,
				username: ((request as any).user as IUser).username,
				password: request.body.password,
				useSSL: true,
				port: 443,
			}),
		)

		// Response with the .mobileconfig file.
		response.contentType("application/x-apple-aspen-config")
		response.setHeader(
			"Content-Disposition",
			`attachment; filename="${
				((request as any).user as IUser).username
			}@${request.hostname}.mobileconfig"`,
		)
		// TODO - investigate signing the profile.
		response.send(mobileconfig.generatePropertyList(profile))
		response.end()
	},
)
