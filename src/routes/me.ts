import * as express from "express"

import { getUser } from "../lib"
import { IUser } from "../front/src/models"

export const meRouter = express.Router()

// /me
meRouter.route("/").get(async (request, response, next) => {
	const user = (request as any).user as IUser

	try {
		if (user) {
			response.json(await getUser(user.username))
			response.end()
		} else {
			next({ code: 401, message: "User is not connected" })
		}
	} catch (error: any) {
		next(error)
	}
})
