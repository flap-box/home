import * as fs from "fs-extra"
import * as express from "express"

import { isAdmin, readFile } from "../tools"

// /restart
export const restartRouter = express.Router()
	.post("/:type", isAdmin, async (request, response, next) => {
		if (request.params.type !== "services" && request.params.type !== "host") {
			return next({
				code: 400,
				message: new Error("Unknown restart type"),
			})
		}


		try {
			await fs.stat(`/var/lib/flap/data/restart.txt`)
			return next({
				code: 400,
				message: new Error("A restart is already in progress"),
			})
		} catch { }

		await fs.writeFile(`/var/lib/flap/data/restart.txt`, request.params.type)

		response.end()
	})
	.get("/:type/state", isAdmin, async (request, response, next) => {
		if (request.params.type !== "services" && request.params.type !== "host") {
			return next({
				code: 400,
				message: new Error("Unknown restart type"),
			})
		}

		try {
			await fs.stat(`/var/lib/flap/data/restart.txt`)
		} catch {
			return response.json({ state: "free" })
		}

		const type = await readFile(`/var/lib/flap/data/restart.txt`)

		switch (type) {
			case "services":
				return response.json({ state: "ongoing" })
			case "host":
				return response.json({ state: "locked" })
		}

		return response.json({ state: "unknown" })
	})
