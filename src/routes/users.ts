import * as express from "express"
import { body, validationResult, param } from "express-validator"

import {
	searchUsers,
	createUser,
	deleteUser,
	getUser,
	updateUser,
	updatePassword,
} from "../lib"
import {
	formatValidationErrors,
	isAdmin,
	isAdminOrUser,
	isAdminOrSetup,
} from "../tools"
import { IUser } from "../front/src/models"

export const usersRouter = express.Router()

// Middlewares
usersRouter.param("userId", async (request, _response, next) => {
	try {
		// Validate the userId
		await param("userId")
			.trim()
			.isAlpha()
			.isLowercase()
			.isLength({ min: 1, max: 32 })
			.run(request)

		validationResult(request).throw()
	} catch (error: any) {
		return next(formatValidationErrors(error))
	}

	// Add the user object to the request
	try {
		const user = await getUser(request.params.userId)
		;(request as any).targetedUser = user
		next()
	} catch (error: any) {
		next({ code: 404, message: error })
	}
})

// /users
usersRouter
	.route("/")
	.get(isAdminOrSetup, async (_request, response, next) => {
		try {
			response.json(await searchUsers())
			response.end()
		} catch (error: any) {
			next(error)
		}
	})
	.post(
		isAdminOrSetup,
		// Check that all the needed properties are there and valid
		body("username")
			.optional()
			.trim()
			.isAlpha()
			.isLowercase()
			.isLength({ min: 1, max: 32 })
			.not()
			.equals("admin"),
		body("fullname")
			.trim()
			.escape()
			.isLength({ min: 3, max: 64 }),
		body("password")
			.optional()
			.isLength({ min: 8, max: 63 }),
		body("email")
			.trim()
			.isLength({ max: 256 })
			.isEmail()
			// Do not apply all sanitize option as it leads to incomprehension.
			.normalizeEmail({gmail_remove_dots: false, gmail_convert_googlemaildotcom: false, yahoo_remove_subaddress: false}),
		body("admin").isBoolean(),
		async (request, response, next) => {
			try {
				validationResult(request).throw()
			} catch (error: any) {
				return next({
					code: 400,
					message: formatValidationErrors(error),
				})
			}
			console.log(request.body)
			try {
				await createUser(request.body)
				response.status(201)
				response.end()
			} catch (error: any) {
				next(error)
			}
		},
	)

// /users/:userId
usersRouter
	.route("/:userId")
	.get(
		isAdminOrUser(request => request.params.userId),
		async (request, response) => {
			response.json((request as any).targetedUser)
			response.end()
		},
	)
	.delete(isAdmin, async (request, response, next) => {
		try {
			if (
				((request as any).user as IUser).username ===
				request.params.userId
			) {
				throw new Error("Can't delete current user")
			}

			await deleteUser(request.params.userId)
			response.end()
		} catch (error: any) {
			next(error)
		}
	})
	.patch(
		isAdminOrUser(request => request.params.userId),
		// Check that submited properties are valid
		body("fullname")
			.optional()
			.isLength({ min: 3, max: 64 })
			.escape(),
		body("email")
			.optional()
			.isLength({ max: 256 })
			.isEmail()
			// Do not apply all sanitize option as it leads to incomprehension.
			.normalizeEmail({gmail_remove_dots: false, gmail_convert_googlemaildotcom: false, yahoo_remove_subaddress: false}),
		body("admin")
			.optional()
			.isBoolean(),
		async (request, response, next) => {
			// Validate values
			try {
				validationResult(request).throw()
			} catch (error: any) {
				return next({
					code: 400,
					message: formatValidationErrors(error),
				})
			}

			// Update the user's info
			try {
				const user = (request as any).user
				if (user.username === request.params.userId) {
					// The user can't update its own `admin` property
					delete request.body.admin
				}

				response.json(
					await updateUser(request.params.userId, request.body),
				)
				response.end()
			} catch (error: any) {
				next(error)
			}
		},
	)

// /users/:userId/password
usersRouter.route("/:userId/password").patch(
	isAdminOrUser(request => request.params.userId),
	// Check that submited properties are valid
	body("oldPassword").isLength({ min: 8, max: 63 }),
	body("newPassword").isLength({ min: 8, max: 63 }),
	async (request, response, next) => {
		// Validate values
		try {
			validationResult(request).throw()
		} catch (error: any) {
			return next({
				code: 400,
				message: formatValidationErrors(error),
			})
		}

		// Update the user's info
		try {
			response.json(
				await updatePassword(request.params.userId, request.body),
			)
			response.end()
		} catch (error: any) {
			next(error)
		}
	},
)
