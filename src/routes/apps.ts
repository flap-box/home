import * as express from "express"
import { IApp } from "../lib"

const FLAP_SERVICES = process.env.FLAP_SERVICES || ""

export const appsRouter = express
	.Router()

	// /apps
	.get("/", async (request, response) => {
		const hostname = `${request.hostname.replace("home.", "")}`

		response
			.json(
				[
					{
						id: "nextcloud",
						name: "Nextcloud",
						enabled: true,
						subdomain: 'files',
						icon: `https://files.${hostname}/nextcloud_medias/nextcloud-logo-white-transparent.png`,
					},
					{
						id: "sogo",
						name: "Sogo",
						enabled: true,
						subdomain: 'mail',
						icon: `https://mail.${hostname}/SOGo.woa/WebServerResources/img/sogo-full.svg`,
					},
					{
						id: "matrix",
						name: "Element",
						enabled: true,
						subdomain: 'chat',
						path: `#/login`,
						icon: `https://chat.${hostname}/matrix_medias/logo.svg`,
					},
					{
						id: "jitsi",
						name: "Jitsi",
						enabled: true,
						subdomain: 'jitsi',
						icon: `https://jitsi.${hostname}/jitsi_medias/logo.png`,
					},
					{
						id: "weblate",
						name: "Weblate",
						enabled: true,
						subdomain: 'weblate',
						icon: `https://docs.weblate.org/en/weblate-4.0.1/_static/logo-128.png`,
					},
					{
						id: "matomo",
						name: "Matomo",
						enabled: true,
						subdomain: 'analytics',
						icon: `https://analytics.${hostname}/matomo_medias/logo.png`,
					},
					{
						id: "peertube",
						name: "Peertube",
						enabled: true,
						subdomain: 'video',
						path: `login`,
						icon: `https://video.${hostname}/peertube_medias/logo.png`,
					},
					{
						id: "funkwhale",
						name: "Funkwhale",
						enabled: true,
						subdomain: 'music',
						path: "login",
						icon: `https://music.${hostname}/funkwhale_medias/logo.png`,
					},
					{
						id: "mailman",
						name: "Mailman",
						enabled: true,
						subdomain: 'lists',
						path: 'accounts/login/',
						icon: `https://lists.${hostname}/mailman_medias/logo.png`,
					},
					{
						id: "wordpress",
						name: "Wordpress",
						enabled: true,
						subdomain: 'blog',
						icon: `https://blog.${hostname}/favicon`,
					}
				].filter(app => FLAP_SERVICES.includes(app.id)) as IApp[],
			)
			.end()
	})
