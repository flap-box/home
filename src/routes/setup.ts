import * as express from "express"

import { getSetupProgress } from "../lib/setup"

export const setupRouter = express
	.Router()

	// /setup
	.get("/", async (_request, response, next) => {
		try {
			response.json({
				progress: await getSetupProgress(),
			})
			response.end()
		} catch (error: any) {
			next(error)
		}
	})
