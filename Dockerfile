# BUILD STAGE
FROM node:lts-slim as builder
WORKDIR /opt/home
# Build dependencies
RUN apt-get update
RUN apt-get install -y build-essential python3 chromium
COPY ./ /opt/home
# SERVER
RUN npm ci
RUN npm run build
# FRONT
WORKDIR /opt/home/src/front
RUN npm ci
RUN npm run build

# RUNTIME STAGE
FROM node:lts-slim
EXPOSE 9000
WORKDIR /opt/home
COPY --from=builder /opt/home/build /opt/home/build
COPY --from=builder /opt/home/package.json /opt/home/package.json
COPY --from=builder /opt/home/package-lock.json /opt/home/package-lock.json
COPY --from=builder /opt/home/src/templates /opt/home/src/templates
COPY --from=builder /opt/home/src/front/dist /opt/home/src/front/dist
RUN npm ci --only=production && \
    chown -R node:node /opt/home && \
    apt-get update && \
    # Runtime dependencies
    apt-get install -y whois && \
    # Reduce final image size
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
USER node
CMD ["npm", "start"]
